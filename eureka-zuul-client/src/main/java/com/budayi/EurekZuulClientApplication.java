package com.budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author LIBOFEI
 * @ClassName EurekZuulClientApplication
 * @description zuul采用类似springMVC的DispatchServlet来实现的，采用的是异步阻塞模型，所以性能比nginx差
 *               zuul一般都是由集群的形式存在的，当负载过高时，可以添加实例来解决性能瓶颈
 * @date 2020/9/12 14:44
 * @Version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy //开启zuul网关功能
public class EurekZuulClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekZuulClientApplication.class,args);
    }
}
