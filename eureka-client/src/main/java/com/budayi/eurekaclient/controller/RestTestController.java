package com.budayi.eurekaclient.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIBOFEI
 * @ClassName RestController
 * @description
 * @date 2020/9/11 13:58
 * @Version 1.0
 **/
@RestController
public class RestTestController {

    static RestTemplate template = new RestTemplate();

    @GetMapping("testRest")
    public String testRest(){
        return template.getForObject("https://www.baidu.com",String.class);
    }
}
