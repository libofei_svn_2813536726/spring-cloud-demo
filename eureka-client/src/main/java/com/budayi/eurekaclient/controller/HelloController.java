package com.budayi.eurekaclient.controller;

import com.netflix.client.http.HttpRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author LIBOFEI
 * @ClassName HelloController
 * @description
 * @date 2020/9/11 10:27
 * @Version 1.0
 **/
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String hello(HttpServletRequest request, @RequestParam("name")String name){
        return "you from " + request.getRemoteAddr() + name + ",i am from " + port;
    }

}
