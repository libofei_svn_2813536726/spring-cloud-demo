package com.budayi.config;

import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author LIBOFEI
 * @ClassName FeignConfig
 * @description
 * @date 2020/9/11 16:28
 * @Version 1.0
 **/
@Configuration
public class FeignConfig {

    /** *
     注入一个beanName为FeignRetryer的bean；注入该bean之后,feign在远程调用失败后会进行重试

     重写FeignClientsConfiguration类中的bean，覆盖掉默认的配置bean，从而达到自定义配置的目的，
    **/
    @Bean
    public Retryer feignRetryer(){
        return new Retryer.Default(100, TimeUnit.SECONDS.toMillis(1),5);
    }
}
