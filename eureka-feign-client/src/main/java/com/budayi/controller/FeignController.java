package com.budayi.controller;

import com.budayi.service.HiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LIBOFEI
 * @ClassName Controller
 * @description
 * @date 2020/9/11 16:42
 * @Version 1.0
 **/
@RestController
public class FeignController {

    @Autowired
    private HiService hiService;

    //通过访问此接口，发现是可以实现负载均衡的
    @GetMapping("/feign/client")
    public String sayHiFromClient(@RequestParam("name")String name){
        return hiService.sayHi(name);
    }
}
