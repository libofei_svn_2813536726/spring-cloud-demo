package com.budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @author LIBOFEI
 * @ClassName EurekaFeignClientApplication
 * @description
 * @date 2020/9/11 16:23
 * @Version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient //开启eurekaClient功能
@EnableFeignClients //开启FeignClient的功能 远程调用其他服务
public class EurekaFeignClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaFeignClientApplication.class,args);
    }

}
