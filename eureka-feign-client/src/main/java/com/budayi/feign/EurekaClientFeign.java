package com.budayi.feign;

import com.budayi.config.FeignConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author LIBOFEI
 * @ClassName EurekaClientFeign
 * @Description
 * @date 2020/9/11 16:27
 * @Version 1.0
 **/

//value表示要调用的服务
@FeignClient(value = "eureka-client",configuration = FeignConfig.class)
public interface EurekaClientFeign {

    @GetMapping(value = "hello")
    String sayHiFromClientEureka(@RequestParam(value = "name")String name);
}
