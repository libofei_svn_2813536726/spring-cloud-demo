package com.budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

/**
 * @author LIBOFEI
 * @ClassName EurekaMoninorClient
 * @description 当服务很多时，监控很不方便，turbine用于聚合多个Dashboard，将所有的组件放到一个页面上进行展示，集中监控
 *               127.0.0.1:8771/turbine.stream
 * @date 2020/9/12 13:51
 * @Version 1.0
 **/
@SpringBootApplication
@EnableTurbine
public class EurekaMoninorClient {

    public static void main(String[] args) {
        SpringApplication.run(EurekaMoninorClient.class,args);
    }
}
