package com.budayi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIBOFEI
 * @ClassName RibbonService
 * @description
 * @date 2020/9/11 14:38
 * @Version 1.0
 **/
@Service
public class RibbonService {

    @Autowired
    RestTemplate template;

    public String hello(String name){
        return template.getForObject("http://eureka-client/hello?name=" + name,String.class);
    }
}
