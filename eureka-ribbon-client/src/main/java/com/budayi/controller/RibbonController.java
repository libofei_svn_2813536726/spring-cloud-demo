package com.budayi.controller;

import com.budayi.service.RibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LIBOFEI
 * @ClassName RibbonController
 * @description
 * @date 2020/9/11 14:40
 * @Version 1.0
 **/
@RestController
public class RibbonController {

    /*** *
    此时的ribbon获取的服务注册列表信息，是基于client获取的，并将服务注册列表信息缓存了一份，从而实现负载均衡
     新建了项目，在ribbon-no-client中演示禁止调用client获取注册列表
    **/
    @Autowired
    private RibbonService ribbonService;

    @GetMapping("hi")
    public String hi(@RequestParam(value = "name",defaultValue = "不达意")String name){
        return ribbonService.hello(name);
    }

    @Autowired //负载均衡器的核心类为LoadBalancerClient,它可以获取负载均衡的服务提供者的实例信息
    private LoadBalancerClient loadBalancerClient;

    @GetMapping("/testRibbon")
    public String hi(){
        ServiceInstance instance = loadBalancerClient.choose("eureka-client");
        return instance.getHost() + ",:" + instance.getPort();
    }

}
