package com.budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author LIBOFEI
 * @ClassName EurekaRibbonClientApplication
 * @description
 * @date 2020/9/11 14:34
 * @Version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient
public class EurekaRibbonClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaRibbonClientApplication.class,args);
    }
}
