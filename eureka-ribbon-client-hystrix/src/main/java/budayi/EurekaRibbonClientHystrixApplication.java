package budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author LIBOFEI
 * @ClassName EurekaRibbonClientApplication
 * @description 本章节是在eureka-ribbon-client上增加了熔断器
 * @date 2020/9/11 14:34
 * @Version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableHystrix //开启熔断器功能
public class EurekaRibbonClientHystrixApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaRibbonClientHystrixApplication.class,args);
    }
}
