package com.budayi.service;

import com.budayi.feign.EurekaClientFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author LIBOFEI
 * @ClassName HiService
 * @description
 * @date 2020/9/11 16:38
 * @Version 1.0
 **/
@Service
public class HiService {

    @Autowired
    private EurekaClientFeign eurekaClientFeign;

    public String sayHi(String name){
        return eurekaClientFeign.sayHiFromClientEureka(name);
    }
}
