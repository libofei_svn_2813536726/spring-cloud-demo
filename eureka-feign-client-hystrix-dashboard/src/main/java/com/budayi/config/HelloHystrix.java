package com.budayi.config;

import com.budayi.feign.EurekaClientFeign;
import org.springframework.stereotype.Component;

/**
 * @author LIBOFEI
 * @ClassName HelloHystrix
 * @description
 * @date 2020/9/12 11:01
 * @Version 1.0
 **/
@Component
public class HelloHystrix implements EurekaClientFeign {

    @Override
    public String sayHiFromClientEureka(String name) {
        return "hello," + name + ",sorry,error from feign";
    }
}
