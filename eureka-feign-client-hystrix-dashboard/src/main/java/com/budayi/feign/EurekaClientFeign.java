package com.budayi.feign;

import com.budayi.config.FeignConfig;
import com.budayi.config.HelloHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author LIBOFEI
 * @ClassName EurekaClientFeign
 * @Description
 * @date 2020/9/11 16:27
 * @Version 1.0
 **/

//value表示要调用的服务
//配置快速失败的处理类，该处理类是feign熔断器的逻辑处理类，
// 需要实现被@feignClient修饰的接口，例如HelloHystrix类就必须实现本接口，然后需要以springBean的形式注入ioc容器
@FeignClient(value = "eureka-client",configuration = FeignConfig.class,fallback = HelloHystrix.class)
public interface EurekaClientFeign {

    @GetMapping(value = "hello")
    String sayHiFromClientEureka(@RequestParam(value = "name") String name);
}
