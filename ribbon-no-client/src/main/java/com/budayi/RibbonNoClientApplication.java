package com.budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LIBOFEI
 * @ClassName RibbonNoClientApplication
 * @description
 * @date 2020/9/11 15:01
 * @Version 1.0
 **/
@SpringBootApplication
public class RibbonNoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RibbonNoClientApplication.class,args);
    }
}
