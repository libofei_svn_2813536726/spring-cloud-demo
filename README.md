# spring-cloud-demo

#### 介绍
springCloud入门篇，主要是常用组件的使用，
注册中心eureka、
负载均衡ribbon、
声明式调用feign、
熔断器hystrix、
熔断监控hystrix-dashboard、
熔断聚合监控turbin、
zuul网关

