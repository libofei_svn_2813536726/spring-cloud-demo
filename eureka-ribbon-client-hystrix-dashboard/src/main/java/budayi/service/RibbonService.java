package budayi.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIBOFEI
 * @ClassName RibbonService
 * @description
 * @date 2020/9/11 14:38
 * @Version 1.0
 **/
@Service
public class RibbonService {

    @Autowired
    RestTemplate template;

    @HystrixCommand(fallbackMethod = "helloError")//代表方法启动熔断器功能
    //fallbackMethod 当eureka-client没有响应，判定eureka-client不可用，开启熔断器，最后进入fallbackMethod逻辑，当熔断器打开，之后所有的请求
    //都会进入fallbackmethod方法，这样的好处就是通过快速失败，请求能够用及时得到处理，线程不再阻塞
    public String hello(String name){
        return template.getForObject("http://eureka-client/hello?name=" + name,String.class);
    }

    public String helloError(String name){
        return "hi,"+ name + ",sorry error";
    }
}
