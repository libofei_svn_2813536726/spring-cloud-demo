package budayi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author LIBOFEI
 * @ClassName EurekaRibbonClientApplication
 * @description 本章节是在eureka-ribbon-client-hystrix上增加了熔断器监控，提供了数据监控和友好的图形化展示界面
 *
 *  http://127.0.0.1:8768/hystrix.stream  显示熔断器的数据指标
 *  http://127.0.0.1:8768/hystrix
 *  https://github.com/Netflix/  详细信息可查看官方文档
 * @date 2020/9/11 14:34
 * @Version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableHystrix //开启熔断器功能
@EnableHystrixDashboard//开启熔断器监控功能
public class EurekaRibbonClientHystrixDashBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaRibbonClientHystrixDashBoardApplication.class,args);
    }
}
