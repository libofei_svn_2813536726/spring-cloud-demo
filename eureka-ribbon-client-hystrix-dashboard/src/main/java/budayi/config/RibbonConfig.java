package budayi.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIBOFEI
 * @ClassName RibbonConfig
 * @description
 * @date 2020/9/11 14:36
 * @Version 1.0
 **/
@Configuration
public class RibbonConfig {

    @Bean
    @LoadBalanced //ribbon与Eureka和RestTemplate相结合实现负载均衡
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
